
App({
    onLaunch: function(options) {
        const systemInfo = wx.getSystemInfoSync(), menuButtonInfo = wx.getMenuButtonBoundingClientRect(); // 胶囊按钮位置信息
        this.globalData.navBarHeight = (systemInfo.statusBarHeight + 44) * 2;  // 导航栏高度 = 状态栏高度 + 44
        this.globalData.menuRight = (systemInfo.screenWidth - menuButtonInfo.right) * 2;
        this.globalData.menuBotton = (menuButtonInfo.top - systemInfo.statusBarHeight) * 2;
        this.globalData.menuHeight = menuButtonInfo.height * 2;

        wx.setNavigationBarColor({
          backgroundColor: '#ffffff',
          frontColor: '#000000',
        })
    },
    
    globalData: {
        navBarHeight: 0, // 导航栏高度
        menuRight: 0, // 胶囊距右方间距（方保持左、右间距一致）
        menuBotton: 0, // 胶囊距底部间距（保持底部间距一致）
        menuHeight: 0, // 胶囊高度（自定义内容可与胶囊高度保证一致）
    },
})
