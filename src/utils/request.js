// class WxRequest{

//     defaultProperty = {
//         baseUrl: '',
//         url: '',
//         method: 'GET',
//         data: null,
//         header:{
//             "content-type": "application/json"
//         },
//         timeout: 15000
//     };

//     requestQueue = [];

//     interceptors = {
//         request: (requestConfig) => {
//             let token = wx.getStorageSync('travel');
//             if(token){
//                 requestConfig.header['travel'] = token;
//             }
//             return requestConfig;
//         },

//         response: (response) => {
//             return response;
//         }
//     };

//     constructor(property = {}){
//         this.defaultProperty = Object.assign({}, this.defaultProperty, property);
//     }

//     request(options){
//         if(this.requestQueue.length == 0){
//             wx.showLoading({title: '数据加载...'});  
//         }
//         this.requestQueue.push(1);
        
//         options.url = this.defaultProperty.baseUrl + options.url;
//         options = {...this.defaultProperty, ...options};
        
//         let wxRequest = new Promise((resolve, reject) => {
//             wx.request({
//                 ...this.interceptors.request(options),
//                 success: (result) => resolve(this.interceptors.response(Object.assign({}, result, {config: options}))),
//                 fail: (err) => reject(this.interceptors.response(Object.assign({}, err, {config: options}))),
//                 complete: () => {
//                     this.requestQueue.pop();
//                     if(this.requestQueue.length == 0) wx.hideLoading();
//                 }
//             })
//         })
//         return wxRequest;
//     }

//     get(url, data = {}, config = {}){
//         return this.request(Object.assign({url, data, method: "GET"}, config));
//     }

//     post(url, data = {}, config = {}){
//         return this.request(Object.assign({url, data, method: "POST"}, config));
//     }

//     delete(url, data = {}, config = {}){
//         return this.request(Object.assign({url, data, method: "DELETE"}, config));
//     }

//     put(url, data = {}, config = {}){
//         return this.request(Object.assign({url, data, method: "PUT"}, config));
//     }

//     parallelRequest(...requestList){
//         return Promise.all(requestList);
//     }

//     uploadFile(url, filePath, name = "file"){
//         return new Promise((resolve, reject) => {
//             wx.uploadFile({
//                url, filePath, name,
//                success: (result) => resolve(this.interceptors.response(result)),
//                fail: (error) => reject(this.interceptors.response(error))
//             })
//         });
//     }
    
// }

// export default new WxRequest();


function request(url, data, method = 'GET') {
    let token = wx.getStorageSync('authentication');
    return new Promise((resolve, reject) => {
            wx.request({url: url,method: method,data: data,
                header: token.length == 0 || token == null ? {} : {
                    travel: token
                },
                success(res) {
                    if (res.statusCode === 200) {
                        if(res.data.code == 600){
                            wx.navigateTo({
                                url: '/modules/other/pages/user/pages/login/index',
                            })
                            return;
                        }
                        resolve(res.data);
                    } else {
                        reject(res);
                    }
                },
                fail(error) {
                    reject(error);
                }
            });
    });
}
 
module.exports = {
  request: request
};
