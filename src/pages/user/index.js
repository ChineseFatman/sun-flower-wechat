import {request} from '../../utils/request';
import api from '../../utils/api';
Page({
    data:{
        navigate:[{
            name: '全部订单',
            image: './images/icon/订单.png'
        },{
            name: '优惠券',
            image: './images/icon/优惠券.png'
        },{
            name: '我的收藏',
            image: './images/icon/收藏夹.png'
        }, {
            name: '浏览历史',
            image: './images/icon/浏览记录.png'
        },{
            name: '修改信息',
            image: './images/icon/修改个人信息.png'
        }],
        list:[],
        userInfo:{},
        cityName: ''
    },
    onShow(){
        if(!wx.getStorageSync('authentication')){
            wx.navigateTo({url: '/modules/other/pages/user/pages/login/index'});
            return;
        }
        let userInfo = wx.getStorageSync('userInfo');
        if(userInfo != null && userInfo.length != 0){
            this.setData({userInfo: userInfo}); return;
        }
        this.setData({cityName: wx.getStorageSync('userStorage').city.cityName})
        request(api.USER_INFO).then(result => {
            if(result.code == 200){
                this.setData({userInfo: result.body});
                wx.setStorageSync('userInfo', result.body);
            }else{
                wx.removeStorageSync('authentication');
                wx.navigateTo({url: '/modules/other/pages/user/pages/login/index'});
            }
        })
    },
    toPage(property){
        let type = property.currentTarget.dataset.type;
        switch(type){
            case 0: wx.navigateTo({url: '/modules/other/pages/order/index'}); break;
            case 2: wx.navigateTo({url: '/modules/other/pages/user/pages/collect/index'}); break;
            case 3: wx.navigateTo({url: '/modules/other/pages/user/pages/history/index'}); break;
            case 4: wx.navigateTo({url: '/modules/other/pages/user/pages/updateUserInfo/index'}); break;
        }
    }

})