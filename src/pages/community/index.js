import {request} from '../../utils/request';
import api from '../../utils/api';

Page({
    data: {
        active: 0,
        list:[],
        searchContent: '',
        city: {},
        category:[],
        currentType: '',
        scrollHeight: 0
    },
    onChange(event){
        this.getArticleList(this.data.category[event.detail.index].code, false);
    },
    onSearch(){
        
    },
    onLoad(property){
        wx.getStorage({key: 'userStorage', success: result => {
            this.setData({city: result.data.city});
            this.getCategoryList(result.data.city.cityName, property.type)
        }});
    },
    getCategoryList(cityName, type){
        request(api.COMMUNITY_CATEGORY_LIST, {cityName: cityName}).then(result => {
            if(result.code == 200) {
                this.setData({category: result.body});
                this.getArticleList(type == null ? result.body[0].code : type, false);
                if(type != null){
                    result.body.forEach((item, index) => {
                        if(item.code == type) this.setData({active: index});
                    })
                }
            }
        })
    },
    getArticleList(type, flag){
        wx.showLoading({
          title: '加载中...',
        })
        this.setData({currentType: type});
        request(api.COMMUNITY_LIST, {
            type: type,
            cityName: this.data.city.cityName,
            size: 10
        }).then(result => {
            if(result.code == 200){
                if(flag) {
                    this.data.list.push(...result.body);
                    this.setData({list: this.data.list});
                }else{
                    this.setData({list: result.body});
                }
            }
            wx.hideLoading()
        })
    },
    onReachBottom(){
        this.getArticleList(this.data.currentType, true);
    },
    onPageScroll(e){
        this.setData({scrollHeight: e.scrollTop});
    },

})