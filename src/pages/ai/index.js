import {request} from '../../utils/request';
import api from '../../utils/api';
Page({

  data: {
    sessionList: [],
    defaultSelect:['长沙有哪些特产？', '给帮我做一份旅行规划吧。', '最近长沙有哪些电影上映吗？', '请帮我做一份辣椒炒肉的菜谱。', '我想考清华需要付出什么样的努力？'],
  },

  onShow(options) {
    wx.setNavigationBarTitle({
        title: '智能小T',
      });
      wx.setNavigationBarColor({
        frontColor: '#ffffff',
        backgroundColor: '#000000',
      })
     this.getSessionList();
  },
  
  deleteHistory(param){
      wx.showModal({
        title: '提示',
        content: '是否要删除当前对话记录？',
        complete: (res) => {
          if (res.cancel) {
                wx.showToast({
                  title: '已取消',
                  icon:'none'
                })
          }
          if (res.confirm) {
              request(api.AI_DELETE_SESSION ,{sessionId: param.currentTarget.dataset.id}).then(result => {
                    if(result.code == 200){
                        wx.showToast({
                            title: '删除成功',
                            icon: 'success'
                        })
                        this.getSessionList();
                    }else wx.showToast({
                        title: '删除失败',
                        icon: 'error'
                    })
              })
          }
        }
      })
  },
  getSessionList(){
    request(api.AI_SESSION_LIST).then(result => {
        if(result.code == 200){
            result.body.forEach(item => {
                item.createTime = item.createTime.substring(0, 10);
            })
        } this.setData({sessionList: result.body});
    })
  },
  createSession(event){
        let question = event.currentTarget.dataset.question;
      request(api.AI_CREATE_SESSION, {}, 'POST').then(result => {
          if(result.code == 200) {
            wx.showToast({
                title: '会话创建成功',
                icon: 'success'
            });
            if(question == null) wx.navigateTo({url: '/modules/chat/pages/chat/index?sessionId=' + result.body})
            else wx.navigateTo({url: '/modules/chat/pages/chat/index?sessionId=' + result.body + "&question=" + question});
          }else wx.showToast({
            title: '会话创建失败',
            icon: 'error'
          })
      })
  }

})