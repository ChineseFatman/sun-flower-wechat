import {request} from '../../utils/request.js';
import api from '../../utils/api.js';
Page({
    data:{
        navigationList: [{
            name: '酒店',
            icon: '../../assest/index/icon/hotel.png',
            background: '#FA7053',
            to: 'pages/hotel/index'
        },{
            name: '机票',
            icon: '../../assest/index/icon/airpot.png',
            background:'#4394FA'
        },{
            name: '火车票',
            icon: '../../assest/index/icon/train.png',
            background: '#6089FF'
        },{
            name: '美食',
            icon: '../../assest/index/icon/food.png',
            background: '#43D0B4'
        },{
            name: '周边',
            icon: '../../assest/index/icon/strategy.png',
            background: '#FF983F'
        }],
        status: 2,
        list:[],
        aroundList:[],
        property: [{
            id: 0,
            left: '50rpx',
            destination: '北京',
          },{
            id: 1,
            left: '180rpx',
            position: '长沙',
            positionCode: "CSX",
            destination: '北京',
            destinationCode: 'BJS',
            type: 1
          },{
            id: 2,
            left: '330rpx',
            position: '长沙',
            positionCode: 'CSQ',
            destination: '北京',
            destinationCode: 'BJP',
            type: -1
          }],
        searchContent:'',
        city: {
            cityEnName: "Changsha",
            cityId: 148,
            cityName: "长沙",
            hot: true,
            id: 159,
            latitude: 28.251818,
            letter: "C",
            longitude: 113.087559,
            parentName: "湖南"
        },
        communityList:[]
    },

    changeStatus(target){
        let index = target.currentTarget.dataset.index;
        if(index < 3) {
            this.setData({status: target.currentTarget.dataset.index}); return;
        }
        if(index == 3){
            wx.reLaunch({
              url: '/pages/community/index?type=tourphoto_all_food',
            })
        }else
            wx.navigateTo({ url: '/modules/around/pages/index/index'})
    },

    onLoad(){
        let property = wx.getStorageSync('property');
        if(property == null || property == '') wx.setStorage({key: 'property', data: this.data.property});
        this.getAroundList();
        this.getCityInfo();
    },

    getCityInfo(){
        let userStorage = {}, that = this;
        wx.getFuzzyLocation({
            type: 'wgs84',
            success (res) {
                userStorage.latitude = res.latitude, userStorage.longitude = res.longitude;
                request(api.COMMUINTY_CITY_CURRENT, {
                    longitude: res.longitude,
                    latitude: res.latitude
                }).then(result => {
                    if(result.code != 200){
                        wx.showToast({
                          title: '获取地理位置失败',
                          icon: 'none'
                        });
                        return;
                    }
                    userStorage.city = result.body;
                    that.setData({city: userStorage.city});
                    wx.setStorage({key: 'userStorage', data: userStorage});
                    that.getAroundList();
                    that.getCommunity(false);
                })
            }
        })
    },
    getAroundList(){
        request(api.AROUND_HOT + "/199/3").then(result => {
            if(result.code != 200){
                wx.showToast({
                  title: result.message,
                  icon: 'error'
                })
                return;
            }
            this.setData({aroundList: result.body});
        })
    },
    getCommunity(flag){
        request(api.COMMUNITY_LIST, {
            type: 'tourphoto_all',
            cityName: this.data.city.cityName,
            size: 10
        }).then(result => {
            if(result.code == 200) {
                if(flag) {
                    this.data.communityList.push(...result.body);
                    this.setData({communityList: this.data.communityList});
                }else{
                    this.setData({communityList: result.body});
                }
            }
        })
    },
    onSearch(){
        
    },
    onReachBottom(){
        if(this.data.communityList.length == 0) return;
        this.getCommunity(true);
    },
    toAroundDetails(event){
        let productNo = event.currentTarget.dataset.productno, cityId = event.currentTarget.dataset.cityid;
        wx.navigateTo({
          url: '/modules/around/pages/details/index?productNo=' + productNo + "&cityId="+ cityId,
        });
    }

    
})