Component({

  properties: {
        status: {
            type: Number,
            value: 0
        }
  },

  data:{
      current:{
        destination: "北京",
        destinationCode: "BJP",
        id: 2,
        left: "330rpx",
        position: "长沙",
        positionCode: "CSQ",
        type: -1
      },
      startTime: (new Date().getMonth() + 1 + "月") + (new Date().getDate() + 1 + '日'),
      endTime: '',
      show: false,
      timeFlag: 'startTime',
      dayFlag: '明天',
  },

  observers:{
    status: function(value){
        wx.getStorage({key: 'property', success: result => {
            if(value < 3) this.setData({current: result.data[value]});
        }})
        this.setData({endTime: ''});
    }
  },

  methods:{
    changeTrainType(type){
        this.setData({'current.type': type.detail});
    },
    changeDistination(){
        let newDestination = this.data.current.destination;
        this.data.current.destination = this.data.current.position;
        this.data.current.position = newDestination;
        this.setData({current: this.data.current})
    },
    onClose(){
        this.setData({show: false});
    },
    selectDataTime(type){
        this.setData({ show: true, timeFlag: type.currentTarget.dataset.type});
    },
    onConfirm(event){
        let date = new Date(event.detail);
        if(this.data.timeFlag === 'startTime'){
            this.setData({startTime: `${date.getMonth() + 1}月${date.getDate()}日`});
        }else{
            this.setData({endTime: `${date.getMonth() + 1}月${date.getDate()}日`});
        }
        let start = this.data.startTime.match(/\d+/g)[1];
        // if(this.data.current.id == 1){
            let end = this.data.endTime.match(/\d+/g)[1];
            if(end < start){
                wx.showToast({
                  title: "出发日期晚于返回日期！",
                  icon: "none"
                });
                return;
            }
        // }
        this.updateDayFlag(Math.abs(start - new Date().getDate()));
        this.onClose();
    },

    updateDayFlag(number){
        let result = "";
        switch(number){
            case 0 : result = "今天"; break;
            case 1 : result = "明天"; break;
            case 2 : result = "后天"; break;
            default: result = "多天后"; break;
        }
        this.setData({dayFlag: result});
    },

    changePlanModel(number){
        this.data.current.type = parseInt(number.currentTarget.dataset.number);
        this.setData({current: this.data.current});
    },

    toSelectCity(event){
        if(event.currentTarget.dataset.id == 0){
            wx.navigateTo({
                url: '/modules/other/pages/search/index?type=' + (event.currentTarget.dataset.id) + 
                "&property=" + (event.currentTarget.dataset.property)
            });
        }else
            wx.navigateTo({
                url: '/modules/other/pages/ticket/city/index?type=' + (event.currentTarget.dataset.id) + 
                "&property=" + (event.currentTarget.dataset.property)
            });
    },
    selectAirportOrTrain(){
        wx.navigateTo({
          url: '/modules/ticket/pages/ticket/index?type=' + (this.data.current.id)
          + "&startTime=" + (this.data.startTime) + "&endTime=" + (this.data.endTime)
        })
    },
    selectHotel(){
        if(this.data.endTime.length == 0){
            wx.showToast({
              title: '请选择离开的日期',
              icon:'none'
            })
            return;
        }
        wx.navigateTo({
          url: '/modules/hotel/pages/index/index?startTime=' + this.data.startTime 
          + "&endTime=" + this.data.endTime,
        })
    }
  }


})