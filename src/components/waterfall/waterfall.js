import {request} from '../../utils/request';
import api from '../../utils/api';
Component({
  properties: {
    list: {
        type: Array,
        value: []
    },
    collect:{
        type: Boolean,
        value: false
    }
  },
  methods:{
    toDetails(event){
        wx.navigateTo({
          url: '/modules/other/pages/article/index?articleId=' + event.currentTarget.dataset.articleid,
        })
    },
    cancelCollect(property){
        wx.showModal({
          title: '提示',
          content: '是否删除当前选中的收藏？',
          complete: (res) => {
            if (res.confirm) {
                request(api.COLLECT, {articleId: property.currentTarget.dataset.articleid}, "POST").then(result => {
                    if(result.code == 200){
                        wx.showToast({
                          title: '取消收藏',
                          icon: 'success'
                        })
                        let list = this.properties.list.filter(item => item.articleId != property.currentTarget.dataset.articleid);
                        this.setData({list: list});
                    }
                })
            }
          }
        })
        console.log(property);
        if(this.properties.collect){

        }
    }
  }

})