import {request} from '../../../../utils/request';
import api from '../../../../utils/api';
Page({
    data:{
        article:{},
        recommendList:[],
        sms:'',
        commentList:[]
    },
    onLoad(event){
        this.getArticleInfo(event.articleId);
    },
    getArticleInfo(articleId){
        request(api.ARTICLE_DETAILS, {articleId: articleId}).then(result => {
            if(result.code == 200) {
                if(result.body.details.images != null){
                    result.body.details.images = result.body.details.images.split(",");
                }
                this.setData({article: result.body});
                this.getRecommendList();
                this.getCommentList();
                this.history();
            }
        })
    },
    getRecommendList(){
        request(api.COMMUNITY_LIST, {
            type: this.data.article.categoryCode,
            cityName: this.data.article.details.address,
            size: 10
        }).then(result => {
            if(result.code == 200) this.setData({recommendList: result.body});
        })
    },
    onClickLeft(){
        wx.navigateBack({delta: 1})
    },
    publishComment(){
        if(this.data.sms.trim().length == 0){
            wx.showToast({
              title: '请输入评论的内容',
              icon: 'none'
            })
            return;
        }
        request(api.PUBLISH_COMMENT, {
            articleId: this.data.article.articleId,
            comment: this.data.sms,
            address: wx.getStorageSync('userStorage').city.cityName
        }, "POST").then(result => {
            if(result.code == 200){
                this.setData({sms: ''});
                wx.showToast({
                  title: result.body,
                  icon: 'success'
                })
                this.getCommentList();
            }else wx.showToast({
              title: result.body,
              icon: 'error'
            })
        })
    },
    getCommentList(){
        request(api.COMMENT_LIST + "/" + this.data.article.articleId).then(result => {
            if(result.code == 200) this.setData({commentList: result.body});
        })
    },
    toCollect(){
        request(api.COLLECT, {
            articleId: this.data.article.articleId
        }, "POST").then(result => {
            if(result.code == 200){
                this.setData({'article.details.collect': !this.data.article.details.collect});
                wx.showToast({title: result.body,icon:"success"})
            }
        })
    },
    history(){
        request(api.USER_ADD_HISTORY, {type:2, productNo: this.data.article.articleId}, "POST").then(result => {
            console.log(result.body);
        })
    }

})