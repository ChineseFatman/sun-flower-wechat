import {request} from '../../../../../utils/request';
import api from '../../../../../utils/api';
Page({
    data:{
        searchContent:'',
        list:[],
        params: {},
        show:false,
        searchList:[]
    },
    onSearch(){
        wx.showLoading({
          title: '正在全力搜索...',
        })
        request(api.TICKET_SEARCH_CITY, {
            content: this.data.searchContent,
            type: this.data.params.type == 2 ? 'train' : 'airport'
        }).then(result => {
            if(result.code == 200){
                this.setData({show: true, searchList: result.body});
            }
            wx.hideLoading();
        })
    },
    onLoad(property){
        this.setData({params: property});
        wx.showLoading({
          title: '加载中',
        })
        request(api.TICKET_CITY + "?type=" + (property.type == 2 ? 'train' : 'airport')).then(result => {
            if(result.code == 200){
                let arrays = [];
                for(let name in result.body) arrays.push({key: name, value: result.body[name]});
                this.setData({list: arrays});
            }
            wx.hideLoading();
        })
    },
    select(property){
        let location = wx.getStorageSync('property'), data = property.currentTarget.dataset;
        location[this.data.params.type][this.data.params.property] = data.name;
        location[this.data.params.type][this.data.params.property + "Code"] = data.citycode;
        wx.setStorageSync('property', location);
        wx.reLaunch({
          url: '/pages/index/index',
        })
    },
    onClose(){
        this.setData({show: false});
    }
})