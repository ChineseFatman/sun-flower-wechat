import {request} from '../../../../utils/request';
import api from '../../../../utils/api';
Page({
    data:{
        active: 0,
        orderList:[],
        page: 1,
        size: 10
    },
    onLoad(){
        this.getTransportationOrder(false);
    },
    onClickLeft(){
        wx.navigateBack({delta: 1})
    },
    onChange(event){
        let index = event.detail.index;
        this.setData({active: index, page: 1});
        if(index <= 1) this.getTransportationOrder(false);
        if(index == 2) this.getAroundOrder(false);
        if(index == 3) this.getHotelOrder(false);
    },
    getHotelOrder(flag){
        wx.showLoading({
          title: '加载中...',
        })
        if(!flag) this.setData({list: []})
        request(api.HOTEL_ORDER_LIST, {page: this.data.page, size: this.data.size}).then(result => {
            if(result.code == 200){
                if(flag){
                    if(result.body.length != 0) this.setData({list: this.data.list.push(...result.body)});
                }else
                    this.setData({list: result.body});
            }
            wx.hideLoading();
        })
    },
    getTransportationOrder(flag){
        wx.showLoading({
            title: '加载中...',
          })
        if(!flag) this.setData({list: []})
        let data = this.data;
        request(api.TICKET_ORDER_LIST, {type: data.active + 1, page: data.page, size: data.size}).then(result => {
            if(result.code == 200){
                if(flag){
                    if(result.body.length != 0) this.setData({list: this.data.list.push(...result.body)});
                }else
                    this.setData({list: result.body});
            }
            wx.hideLoading();
        })
    },
    getAroundOrder(flag){
        wx.showLoading({
            title: '加载中...',
        })
        if(!flag) this.setData({list: []})
        request(api.AROUND_ORDER_LIST, {page: 1, size: 10}).then(result => {
            if(result.code == 200){
                result.body.forEach(item => {
                    item.toTime = item.toTime.substring(0, 11);
                })
                if(flag){
                    if(result.body.length != 0) this.setData({list: this.data.list.push(...result.body)});
                }else
                    this.setData({list: result.body});
            }
            wx.hideLoading();
        })
    },
    cancelOrder(property){
        request(api.HOTEL_ORDER_CANCEL, {orderId: property.currentTarget.dataset.orderid}).then(result => {
             if(result.code == 200) {
                 wx.showToast({
                   title: result.body,
                   icon: 'success'
                 })
                 this.updatePage();
             }else wx.showToast({
               title: result.body,
               icon: 'error'
             })
             this.setData({page: 1});
        })
    },
    cancelAround(property){
        wx.showModal({
          title: '提示',
          content: '是否取消当前订单?',
          complete: (res) => {
            if (res.confirm) {
                request(api.AROUND_ORDER_CANCEL, {orderId: property.currentTarget.dataset.aroundid}).then(result => {
                    if(result.code == 200) {
                        wx.showToast({
                          title: result.body,
                          icon: 'success'
                        })
                        this.updatePage();
                    }else wx.showToast({
                      title: result.body,
                      icon: 'error'
                    })
                    this.setData({page: 1});
                })
            }
          }
        })
    },
    onReachBottom(){
        let index = this.data.active;
        this.setData({page: this.data.page + 1});
        if(index <= 1) this.getTransportationOrder(true);
        if(index == 2) this.getAroundOrder(true);
        if(index == 3) this.getHotelOrder(true);
    },

    updatePage(){
        let index = this.data.active;
        if(index <= 1) this.getTransportationOrder(false);
        if(index == 2) this.getAroundOrder(false);
        if(index == 3) this.getHotelOrder(false);
    },
    cancelTicket(property){
        wx.showModal({
            title: '提示',
            content: '是否取消当前订单?',
            complete: (res) => {
              if (res.confirm) {
                  request(api.TICKET_ORDER_CANCEL, {orderId: property.currentTarget.dataset.ticketid}).then(result => {
                      if(result.code == 200) {
                          wx.showToast({
                            title: result.body,
                            icon: 'success'
                          })
                          this.updatePage();
                      }else wx.showToast({
                        title: result.body,
                        icon: 'error'
                      })
                      this.setData({page: 1});
                  })
              }
            }
          })
    }

})