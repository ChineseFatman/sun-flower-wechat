import {request} from '../../../../../../utils/request';
import api from '../../../../../../utils/api';
Page({
    data:{
        active:0,
        list: [],
        page:1,
        size:10
    },
    onClickLeft(){
        wx.navigateBack({delta: 1})
    },
    onLoad(){
        this.getList(false);
    },
    onChange(event){
        this.setData({active: event.detail.index, page: 1});
        this.getList(false);
    },
    getList(flag){
        wx.showLoading({
          title: '正在加载...',
        })
        let index = this.data.active == 0 ? 1 : 2;
        request(api.USER_HISTORY_LIST, {type: index, page: this.data.page,size:this.data.size}).then(result => {
            if(result.code == 200){
                let arrays = [];
                result.body.forEach(item => arrays.push(item.object.body));
                if(flag){
                    this.setData({list: this.data.list.push(...arrays)});
                }else  
                    this.setData({list: arrays});
            }
            wx.hideLoading();
        })
    },
    toDetails(event){
        let productNo = event.currentTarget.dataset.productno, cityId = event.currentTarget.dataset.cityid;
        wx.navigateTo({
          url: '/modules/around/pages/details/index?productNo=' + productNo + "&cityId="+ cityId,
        });
    },
    onReachBottom(){
        this.setData({page: this.data.page + 1});
        this.getList(true);
    }
})