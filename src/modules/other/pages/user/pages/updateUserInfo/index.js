import {request} from '../../../../../../utils/request';
import api from '../../../../../../utils/api';

Page({
    data:{
        avatar: '',
        username: '',
        phone: '',
        identifyCard:'',
        description:''
    },
    onLoad(){
        let user = wx.getStorageSync('userInfo');
        this.setData({
            avatar: user.avatar,
            username: user.username,
        });
    },
    onClickLeft(){
        wx.navigateBack({delta: 1})
    },
    updateUserInfo(){
        let user = this.data;
        if(user.username.length == 0){
            this.taost('请输入正确的用户名'); return;
        }else if(user.phone.length == 0){
            this.taost('请输入正确的手机号'); return;
        }else if(user.identifyCard.length == 0){
            this.taost('请输入正确的身份证'); return;
        }else if(user.description.length == 0){
            this.taost('请输入您的个人描述'); 
        }
        request(api.USER_UPDATE_INFO, user, "post").then(result => {
            wx.showToast({
              title: '修改成功',
              icon: 'success'
            })
            if(result.code == 200) wx.reLaunch({
              url: '/pages/user/index',
            })
        })
    },
    taost(message){
        wx.showToast({
          title: message,
          icon: 'none'
        })
    }
})