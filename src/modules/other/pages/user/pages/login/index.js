import {request} from '../../../../../../utils/request';
import api from '../../../../../../utils/api';
Page({
    data:{
        second: 60, //默认60秒
        showText: true, //判断短信是否发送
        phone: '', //手机号码
        yzm: '', //验证码
        message: ''
    },
    getVertifyCode() {
        if(!/^1[34578]\d{9}$/.test(this.data.phone)){
            wx.showToast({
              title: '手机号格式不正确!',
              icon: 'error'
            })
            return;
        }
        var that = this;
        var interval = setInterval(() => {
            if(this.data.second == 59){
                request(api.USER_VERIFY_CODE, {mobileNumber: this.data.phone}).then(result => {
                    this.setData({message: result});
                })
            }
            that.setData({
                showText: false,
                second: that.data.second - 1
            });
            if(that.data.second <= 0){
                clearInterval(interval);
                that.setData({second: 60, showText: true, message: {}});
            }
        }, 1000);
    },
    wxLogin(){
        wx.showLoading({
          title: '登录中...',
        })
        wx.login({success: (res) => {
            request(api.USER_LOGIN, {
                jsCode: res.code,
                type: 'wx'
            }, "POST").then(result => {
                if(result.code == 200){
                    wx.setStorage({key: 'authentication', data: result.body});
                    wx.switchTab({url: '/pages/index/index'})
                }else{
                    wx.showToast({title: result.body,icon: 'error'})
                }
                wx.hideLoading();
            })
          },fail: err => {
              wx.showToast({title: "微信登录失败",icon: 'error'})
          }
        })  
    },
    toLogin(){
        if(!/^1[34578]\d{9}$/.test(this.data.phone) || this.data.yzm.length == 0){
            wx.showToast({title: '信息填写错误!',icon: 'error'})
            return;
        }
        request(api.USER_LOGIN, {
            phone: this.data.phone,
            verifyCode: this.data.yzm,
            type: 'html'
        }, "POST").then(result => {
            if(result.code == 200){
                wx.setStorage({key: 'authentication', data: result.body});
                wx.switchTab({url: '/pages/index/index'})
            }else{
                wx.showToast({title: result.body,icon: 'error'})
            }
        })
    }

})