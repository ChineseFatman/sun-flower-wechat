import {request} from '../../../../../../utils/request';
import api from '../../../../../../utils/api';
Page({
    data:{
        active:0,
        list: [],
        page:1,
        size:10
    },
    onClickLeft(){
        wx.navigateBack({delta: 1})
    },
    onLoad(){
        this.getList(false);
    },
    onChange(event){
        this.setData({active: event.detail.index, page: 1});
        this.getList(false);
    },
    getList(flag){
        wx.showLoading({
          title: '正在加载...',
        })
        let path = this.data.active == 0 ? api.AROUND_FAVORITE_LIST : api.ARTICLE_COLLECT_LIST
        request(path, {page:this.data.page, size: this.data.size}).then(result => {
            if(result.code == 200){
                if(flag){
                    if(result.body.length != 0) this.setData({list: this.data.list.push(...result.body)});
                }else
                    this.setData({list: result.body});
            }
            wx.hideLoading();
        })
    },
    toDetails(event){
        let productNo = event.currentTarget.dataset.productno, cityId = event.currentTarget.dataset.cityid;
        wx.navigateTo({
          url: '/modules/around/pages/details/index?productNo=' + productNo + "&cityId="+ cityId,
        });
    },
    onReachBottom(){
        this.setData({page: this.data.page + 1});
        this.getList(true);
    },
    cancelCollect(property){
        let data = property.currentTarget.dataset;
        wx.showModal({
          title: '提示',
          content: '是否删除当前选中的收藏？',
          complete: (res) => {
            if (res.confirm) {
               request(api.AROUND_FAVORITE, {
                   cityId: data.cityid,
                   productNo: data.id
               }, "POST").then(result => {
                   if(result.code == 200) {
                       wx.showToast({
                         title: '取消收藏成功',
                         icon: 'success'
                       })
                       this.setData({page: 1})
                       this.getList(false);
                   }
               })
            }
          }
        })
    }
})