import {request} from '../../../../../../utils/request';
import api from '../../../../../../utils/api';
Page({
    data:{
        username: '',
        phone: '',
        password: '',
        sex: '0',
        identifyCard: ''
    },
    onChange(e){
        this.setData({'user.sex': e.detail});
    },
    toRegister(){
        for(let propery in this.data){
            if(this.data[propery].length == 0 || this.data[propery] == null){
                wx.showToast({title: '请将信息填写完整', icon: 'none'}); return;
            }
        } 
        wx.showLoading({
          title: '正在注册...',
          mask: true
        })
        wx.login({success: (res) => {
            this.data.jsCode = res.code;
            request(api.USER_REGISTER, this.data, "POST").then(result => {
                if(result.code == 200){
                    wx.showToast({title: '注册成功，请登录'});
                    wx.navigateTo({
                        url: '/modules/other/pages/user/pages/login/index',
                    })
                }else  wx.showToast({title: result.body, icon: 'error'});
                wx.hideLoading();
            })
          },
        })
    },

})