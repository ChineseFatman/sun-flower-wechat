import {request} from '../../../../utils/request';
import api from '../../../../utils/api';

Page({
    data:{
        value: '',
        obj: {},
        searchContent: '',
        hot: [],
        list: [],
        current: '',
        show:false,
        searchResult:{}
    },
    onLoad(options){
        this.setData({obj: options});
        wx.showLoading({
          title: '加载中...',
        })
        request(api.HOTEL_CITY).then(result => {
            if(result.code == 200){
                let arrays = [];
                for(let name in result.body.list){
                    arrays.push({key: name, value: result.body.list[name]});
                }
                this.setData({hot: result.body.hot, list: arrays});
            }
            wx.hideLoading();
        })
        this.setData({current: wx.getStorageSync('userStorage').city.cityName});
    },
    onSearch(){
        if(this.data.searchContent.trim().length == 0) return;
        wx.showLoading({
          title: '正在全力搜索..',
        })
        let city = wx.getStorageSync('userStorage');
        request(api.HOTEL_CITY_SEARCH, {
            location: this.data.searchContent,
            range: 100,
            latitude: city.latitude,
            longitude: city.longitude
        }, "POST").then(result => {
            if(result.code == 200){
                this.setData({show: true, searchResult: result.body});
            }
            wx.hideLoading();
        })
    },
    onSelect(cityName){
        let property = wx.getStorageSync('property');
        property[this.data.obj.type][this.data.obj.property] = cityName.currentTarget.dataset.cityname;
        wx.setStorageSync('property', property);
        wx.reLaunch({url: '/pages/index/index'});
    },
    onClose(){
        this.setData({show: false});
    }
})