import {request} from '../../../../utils/request';
import api from '../../../../utils/api';
Page({
    data:{
        scenery: null,
        show: true,
        overLayShow: false,
        router:{},
        star: false,
        showCalender: false,
        selectTime: ''
    },
    onClickLeft(){
        wx.navigateBack({delta: 1})
    },
    onLoad(event){
        wx.getStorage({key: 'authentication', fail: result => {
            wx.navigateTo({url: '/modules/other/pages/user/pages/login/index'});
        }, success: result => this.loadData(event)});
    },
    loadData(event){
        wx.showLoading({
          title: '加载中...',
        });
        let productNo = event.productNo, cityId = event.cityId;
        request(api.AROUND_DETAILS + "/" + productNo + "/" + cityId).then(result => {
            if(result.code != 200){
                wx.showToast({
                  title: '网络出现错误，请稍后重试',
                  icon: 'error'
                })
                this.setData({show: true})
                return;
            }
            this.setData({
                scenery: result.body,
                show: false
            });
            this.parseString();
            this.getFavoriateInfo();
            this.addHistory();
            wx.hideLoading();
        })
    },
    parseString(){
        let details = this.data.scenery.details;
        for(let key in details){
            if(typeof(details[key]) == "string" && (details[key].startsWith("[") || details[key].startsWith("{"))){
                details[key] = JSON.parse(details[key]);
            }
        }
        this.setData({'scenery.details': details});
        this.getRouter(details.scheduling.Versions[0].Id);
    },
    onClickHide(){
        this.setData({overLayShow: false})
    },
    onClickShow(){
        this.setData({overLayShow: true});
    },
    change(e){
        let version = this.data.scenery.details.scheduling.Versions[e.detail.index];
        this.getRouter(version.Id);
    },
    getRouter(id){
        request(api.AROUND_ROUTERING + "/" + id).then(result => {
            if(result.code != 200){
                wx.showToast({
                  title: '网络出现错误,请稍后重试',
                  icon:'error'
                })
                return;
            }
            result.body.journeyList = JSON.parse(result.body.journeyList);
            for(let i = 0; i < result.body.journeyList.length; i++){
                result.body.journeyList[i].viewList = result.body.journeyList[i].viewList.slice(0, 2)
            }
            this.setData({router: result.body});
        })
    },
    toFavorite(){
        request(api.AROUND_FAVORITE, {
            cityId: this.data.scenery.cityId,
            productNo: this.data.scenery.id
        }, "POST").then(result => {
            if(result.code == 200){
                this.setData({star: !this.data.star});
                wx.showToast({title: '成功'})
            }else{
                wx.showToast({title: '操作失败',icon: 'error'})
            }
        })
    },
    getFavoriateInfo(){
        request(api.AROUND_has_FAVORITE, {
            cityId: this.data.scenery.cityId,
            productNo: this.data.scenery.id
        }).then(result => {
            if(result.code == 200){
                this.setData({star: result.body});
            }
        })
    },
    selectDay(){
        this.setData({showCalender: true});
    },
    onClose(){
        this.setData({showCalender: false});
    },
    onConfirm(event){
        let date = new Date(event.detail), time = `${date.getMonth() + 1}-${date.getDate()}`;
        wx.showModal({
          title: '提示',
          content: '是否在' + time + '号参加' + (this.data.scenery.mainTitle) + "?",
          complete: (res) => {
            if (res.confirm) {
              this.createOrder(time);
            }
          }
        })
    },
    createOrder(toTime){
        let city = wx.getStorageSync('userStorage').city;
        request(api.AROUND_ORDER_CREATE, {
            productNo: this.data.scenery.id,
            pay: this.data.scenery.tcPrice,
            payCityName: city.cityName,
            portCity: this.data.scenery.city,
            toCity: this.data.scenery.portCity,
            toTime: toTime
        }, "POST").then(result => {
            console.log(result);
            if(result.code == 200){
                wx.showToast({
                  title: result.body,
                  icon: 'success'
                })
                this.setData({showCalender: false}); return;
            }
            wx.showToast({
              title: result.body,
              icon: 'error'
            })
        })
    },
    addHistory(){
        request(api.USER_ADD_HISTORY,{
            type: 1,
            productNo: this.data.scenery.id
        }, "POST").then(result => {
            console.log(result.body);
        })
    }

})