import {request} from '../../../../utils/request';
import api from '../../../../utils/api';
Page({
    data:{
        dayList:[{
            id: 0, name: '全部'
        }, {
            id: 1, name: '一日游'
        },{
            id: 2, name: '两日游'
        }, {
            id: 3, name:'三日游'
        }],
        list: [],
        selectDay: 0,
        searchContent: '',
        page: 1,
        scrollHeight: 0,
        city:{}
    },
    selectDay(event){
        this.setData({selectDay: event.currentTarget.dataset.index});
        this.getList(false);
    },
    onLoad(){
        this.getList(false);
        wx.getStorage({key: "userStorage",success: result => {
            this.setData({city: result.data.city});
        }})
    },
    getList(append){
        request(api.AROUND_LIST, {
            cityId: 199,
            content: this.data.searchContent,
            page: this.data.page,
            size: 10,
            sort: 'Asc',
            sortProperty: 'tcPrice',
            day: this.data.selectDay
        }).then(result => {
            if(result.code != 200){
                wx.showToast({
                  title: '网络出现错误',
                  icon: 'error'
                })
                return;
            }
            if(append){
                this.data.list.push(...result.body);
                this.setData({list: this.data.list});
            }else
                this.setData({list: result.body});
        })
    },

    onReachBottom(){
        this.setData({page: this.data.page + 1});
        this.getList(true);
    },
    onPageScroll(e){
        this.setData({scrollHeight: e.scrollTop});
    },

    toDetails(event){
        let productNo = event.currentTarget.dataset.productno, cityId = event.currentTarget.dataset.cityid;
        wx.navigateTo({
          url: '/modules/around/pages/details/index?productNo=' + productNo + "&cityId="+ cityId,
        });
    },
    onSearch(){
        
    }
    
})