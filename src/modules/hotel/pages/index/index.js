import {request} from '../../../../utils/request';
import api from '../../../../utils/api';

Page({
    data:{
        searchContent:'',
        filterIndex: 0,
        filterList:[],
        filterSubList:[],
        regionIndex: 0,
        regionList:[],
        regionSubList:[],
        condition:[],
        hotelList:[],
        page: 1,
        show: false,
        selectHotel: {},
        startTime:'',
        endTime:''
    },
    onSearch(){
        if(this.data.searchContent.length == 0) return;
        this.getHotelList(false);
    },
    onLoad(property){
        this.setData({
            'startTime': property.startTime.replace("月", '-').replace("日", ''),
            'endTime': property.endTime.replace("月", '-').replace("日", ''),
        })
        this.getHotelList(false);
    },
    getCategory(type){
        wx.showLoading({
          title: '加载中...',
        })
        request(api.HOTEL_CATEGORY, {type: !type}).then(result => {
            if(result.code == 200) {
                if(type) {
                    this.setData({filterList: result.body}); 
                    this.getSubCategoryList(result.body[0].id);
                }else{
                    this.setData({regionList: result.body});
                    this.getRegionSubCategoryList(result.body[0].id);
                }
               
            }
            wx.hideLoading()
        });
    },
    change(event){
        let id = event.target.id;
        this.getCategory(id == 'filter');
    },
    getSubCategoryList(index){
        request(api.HOTEL_CATEGORY_FILTER, {categoryId: index}).then(result => {
            if(result.code == 200) {
                let arrays = [];
                for(let name in result.body){
                    arrays.push({type: name, value: result.body[name]});
                }
                this.setData({filterSubList: arrays});
            }
        })
    },
    getRegionSubCategoryList(index){
        let city = wx.getStorageSync('property')[0].destination;
        request(api.HOTEL_REGION_FILTER, {categoryId: index, cityName: city}).then(result => {
            if(result.code == 200) this.setData({regionSubList: result.body});
        })
    },
    abc(event){
        this.getSubCategoryList(this.data.filterList[event.detail].id);
    },
    def(event){
        this.getRegionSubCategoryList(this.data.regionList[event.detail].id);
    },
    selectFilter(event){
        this.data.condition.push(event.currentTarget.dataset.filterid);
        this.setData({condition: this.data.condition, page: 1});
        this.selectComponent('#filter').toggle();
        this.getHotelList(false);
    },
    selectRegion(event){
        this.data.condition.push(event.currentTarget.dataset.filterid);
        this.setData({condition: this.data.condition, page:1});
        this.selectComponent('#region').toggle();
        this.getHotelList(false);
    },

    getHotelList(flag){
        let city = wx.getStorageSync('property')[0].destination;
        let condition = '';
        for(let i = 0, length = this.data.condition.length; i < length; i++){
            if(i == 0) condition += ("+" + this.data.condition[i]);
            else condition += (",+" + this.data.condition[i]);
        }
        wx.showLoading({
          title: '正在努力加载..',
        })
        let body = {
            cityName: city,
            page: this.data.page,
            size: 10
        }
        if(condition.length != 0) body.filter = condition;
        if(this.data.searchContent.trim().length != 0) body.searchContent = this.data.searchContent;
        request(api.HOTEL_SEARCH, body , "POST").then(result =>{
            if(result.code == 200) {
                result.body.forEach(item => {
                    for(let name in item){
                        if(typeof(item[name]) == "string" 
                        && (item[name].startsWith("[") || item[name].startsWith("{"))) item[name] = JSON.parse(item[name]);
                    }
                })
                if(flag) {
                    this.data.hotelList.push(...result.body);
                    this.setData({hotelList: this.data.hotelList});
                }else
                    this.setData({hotelList: result.body})
            }
            wx.hideLoading();
            this.setData({condition: []});
        })
    },
    onReachBottom(){
        this.setData({page: this.data.page + 1});
        this.getHotelList(true);
    },
    onClose(){
        this.setData({show: false});
    },
    selectHotel(obj){
        let object = obj.currentTarget.dataset.obj;
        object.selectNumber = 1;
        this.setData({
            show: true,
            selectHotel: object
        });
    },
    setPsersonNumber(event){
        this.setData({'selectHotel.selectNumber': event.detail});
    },
    createOrder(){
        let hotel = this.data.selectHotel;
        request(api.HOTEL_CREATE_ORDER, {
            inTime: this.data.startTime,
            outTime: this.data.endTime,
            hotelId: hotel.hotelId,
            personNumber: hotel.selectNumber
        }, "POST").then(result => {
            if(result.code == 200){
                wx.showToast({
                  title: '订购成功',
                  icon: 'success'
                })
                this.setData({show: false});
            }else 
                wx.showToast({
                  title: result.body,
                  icon: 'error'
                })
        })
    }
})